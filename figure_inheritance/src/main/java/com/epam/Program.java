package com.epam;

import java.util.ArrayList;
import java.util.List;

public class Program {
    public static void Main(String[] args){
        Triangle tri1 =  new Triangle("tri1", "grey", 4, 7);
        Triangle tri2 =  new Triangle("tri2", "orange", 2, 6);
        Triangle tri3 =  new Triangle("tri3", "purple", 9, 3);
        Circle circle1 =  new Circle("circle1", "blue", 4);
        Circle circle2 =  new Circle("circle2", "green", 1);
        Circle circle3 =  new Circle("circle3", "red", 6);
        Rectangle rec1 =  new Rectangle("rec1", "blue", 5, 10);
        Rectangle rec2 =  new Rectangle("rec2", "red", 7, 8);
        Rectangle rec3 =  new Rectangle("rec3", "yellow", 11, 2);

        List<Triangle> list_of_tri = new ArrayList<Triangle>(List.of(tri1, tri2, tri3)){};
        list_of_tri.forEach(shape -> {
            System.out.println(shape.color);
            shape.getSquare(shape.height, shape.side);
        });

        List<Circle> list_of_circle = new ArrayList<Circle>(List.of(circle1, circle2, circle3)){};
        list_of_circle.forEach(shape -> {
            System.out.println(shape.color);
            shape.getSquare(shape.radius);
        });

        List<Rectangle> list_of_rec = new ArrayList<Rectangle>(List.of(rec1, rec2, rec3)){};
        list_of_rec.forEach(shape -> {
            System.out.println(shape.color);
            shape.getSquare(shape.side1, shape.side2);
        });
    }
}
