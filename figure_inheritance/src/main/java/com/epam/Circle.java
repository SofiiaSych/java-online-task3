package com.epam;

public class Circle extends Shape{
    int  radius;

    public Circle(String name, String color, int radius) {
        super(name, color);
        this.radius = radius;
    }

    public double getSquare(int radius) {
        return Math.PI*radius*radius;
    }
}
