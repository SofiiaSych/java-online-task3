package com.epam;

public class Triangle extends Shape{
    int height;
    int side;
    public Triangle(String name, String color, int height, int side) {
        super(name, color);
        this.height = height;
        this.side = side;
    }

    public double getSquare(int height, int side) {
        double _square = (height*side)/2;
        return _square;
    }
}
