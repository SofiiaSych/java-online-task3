package com.epam;

public class Rectangle extends Shape{
    int side1;
    int side2;

    public Rectangle(String name, String color, int side1, int side2) {
        super(name, color);
        this.side1 = side1;
        this.side2 = side2;
    }

    public double getSquare(int side1, int side2){
        return side1*side2;
    }
}
