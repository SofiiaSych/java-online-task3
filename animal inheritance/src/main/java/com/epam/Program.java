package com.epam;

import java.util.ArrayList;
import java.util.List;

public class Program {
    public static void main(String[] args){
        Animal cow1 = new Cow("Burenka", 200, "brown", "mammal", 2);
        Animal cow2 = new Cow("Mania", 180, "black_and_white", "mammal", 2);
        Animal lion1 = new Lion("Mufasa", 150, "brown", "mammal", 0);
        Animal lion2 = new Lion("Simba", 160, "light_red", "mammal", 0);
        Animal shark = new Shark("Bruse", 260, "grey_white", "fish", true);
        Animal tune = new Tune("Rio", 80, "grey", "fish", true);
        Animal parrot = new Parrot("Cash", 5, "green", "bird", true);
        Animal eagle = new Eagle("America", 20, "brown", "bird", false);

        List<Animal> list = new ArrayList<Animal>(List.of(cow1,cow2,lion1, lion2,shark, tune,parrot,eagle)) {};

        list.forEach(animal -> {
            if (animal.color=="brown"){
                System.out.println(animal.name);
                animal.Eat();
            }
        });
    }
}
