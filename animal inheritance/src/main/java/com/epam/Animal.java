package com.epam;


public abstract class Animal {
    String name;
    String color;
    String classification;
    int weight;
    public Animal (String name, int weight, String color, String classification){
        this.name = name;
        this.weight = weight;
        this.color = color;
        this.classification = classification;
    }
    public void Eat(){
        System.out.println("eat anything");
    }
    public void Sleep(){
        System.out.println("I`m sleeping");
    }
}
