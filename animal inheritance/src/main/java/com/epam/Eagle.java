package com.epam;

public class Eagle extends Bird{
    public Eagle(String name, int weight, String color, String classification, boolean speak) {
        super(name, weight, color, classification, speak);
    }

    @Override
    public void Eat() {
        System.out.println("I`m eating meat");
    }
}
