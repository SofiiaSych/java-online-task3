package com.epam;

public abstract class Bird extends Animal {
    boolean can_speak_human = false;
    public Bird(String name, int weight, String color, String classification, boolean speak){
        super(name, weight, color, classification);
        speak = can_speak_human;
    }
    public void Fly(){
        System.out.println("I`m flying");
    }
}
