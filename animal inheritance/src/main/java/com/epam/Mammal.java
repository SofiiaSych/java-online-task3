package com.epam;

public abstract class Mammal extends Animal {
    int horn_count = 0;
    public Mammal(String name, int weight, String color, String classification, int horn){
        super(name, weight, color, classification);
        horn = horn_count;
    }
    public void Walk(){
        System.out.println("I`m walking");
    }
}
