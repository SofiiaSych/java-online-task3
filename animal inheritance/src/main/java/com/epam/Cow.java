package com.epam;

public class Cow extends Mammal {
    public Cow(String name, int weight, String color, String classification, int horn) {
        super(name, weight, color, classification, horn);
    }

    @Override
    public void Eat() {
        System.out.println("I`m eating plants");
    }



}
