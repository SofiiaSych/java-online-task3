package com.epam;

public class Shark extends Fish{
    public Shark(String name, int weight, String color, String classification, boolean sea) {
        super(name, weight, color, classification, sea);
    }

    @Override
    public void Eat() {
        System.out.println("I`m eating meat");
    }
}
