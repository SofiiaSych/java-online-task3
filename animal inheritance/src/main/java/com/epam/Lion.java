package com.epam;

public class Lion extends Mammal{
    public Lion(String name, int weight, String color, String classification, int horn) {
        super(name, weight, color, classification, horn);
    }

    @Override
    public void Eat() {
        System.out.println("I`m eating meat");
    }
}
