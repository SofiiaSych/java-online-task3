package com.epam;

public abstract class Fish extends Animal{
    boolean is_sea = false;
    public Fish(String name, int weight, String color, String classification, boolean sea){
        super(name, weight, color, classification);
        sea = is_sea;
    }
    public void Swim(){
        System.out.println("I`m swimming");
    }
}
